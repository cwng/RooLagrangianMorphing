//-------------------------------------------//
//              4amorphing.cxx               //
// Author: Chi Wing Ng                       //
// Date:   Mar 16, 2021                      //
// Morphing Code based on morphing_1d_ctW.py //
// developed by Jordy Degens                 //
//-------------------------------------------//

// io and system includes
#include <iostream>
#include <fstream>

// STL includes
#include <vector>
#include <cmath>
#include <unordered_map>

// ROOT includes
#include "TROOT.h"
#include "TInterpreter.h"
#include "TFile.h"
#include "TFolder.h"
#include "TH1.h"
#include "TMatrixDSym.h"

// RooFit includes
#include "RooAbsReal.h"
#include "RooGlobalFunc.h"
#include "RooArgList.h"
#include "RooStringVar.h"
#include "RooRealVar.h"
#include "RooFormulaVar.h"
#include "RooConstVar.h"
#include "RooArgSet.h"
#include "RooGaussian.h" 
#include "RooWorkspace.h"
#include "RooNumber.h"
#include "RooDataSet.h"
#include "RooMultiVarGaussian.h"
#include "RooProdPdf.h"
#include "RooStats/ModelConfig.h"

// RooLagrangianMorphing
#include "RooLagrangianMorphing/RooLagrangianMorphing.h"

std::string strip(std::string s) {
	std::string whitespaces = " \t\f\v\n\r";
	size_t first = s.find_first_not_of(whitespaces);
	size_t last = s.find_last_not_of(whitespaces);
	s.erase(last+1);
	s.erase(s.begin(), s.begin()+first);
	return s;
}

void replace(std::string & s, const std::string & str_to_remove, const std::string & str_to_insert) {
	size_t pos = s.find(str_to_remove);
	while(pos != std::string::npos) {
		s.replace(pos, str_to_remove.length(), str_to_insert);
		pos = s.find(str_to_remove);
	}
}

int main(int argc, char** argv) {
	
	// set ROOT into batch mode to avoid graphic display
	gROOT->SetBatch(true);
	gInterpreter->GenerateDictionary("vector<RooFormulaVar>","RooFormulaVar.h;vector");

	bool find_every_khi_squared = true;

	// set output directory
	std::string output_dir = __FILE__;
	output_dir = output_dir.substr(0, output_dir.rfind('/'));
	std::cout << "output_dir = " << output_dir << std::endl;

	// input samples for morphing
	std::string input_file = "/eos/user/j/jdegens/morph_inputs/v3/v3_thestat_withSM_val.root";
	std::vector<std::string> samplelist = {"ictW-c0-ctW-c0","ictW-c-10-ctW-c-10","ictW-c-10-ctW-c0.526316","ictW-c-10-ctW-c10","ictW-c-4.73684-ctW-c-10","ictW-c-4.73684-ctW-c1.57895","ictW-c-5.78947-ctW-c-5.78947","ictW-c0.526316-ctW-c-6.84211","ictW-c1.57895-ctW-c-10","ictW-c10-ctW-c-10","ictW-c10-ctW-c-4.73684","ictW-c10-ctW-c3.68421","ictW-c2.63158-ctW-c3.68421","ictW-c4.73684-ctW-c10","ictW-c6.84211-ctW-c-3.68421"};
	std::string observable = "selection_cos_ly_part_inc";

	// push all the input samples in a RooArgList
	std::vector<RooStringVar*> samplenames;
	RooArgList *inputs = new RooArgList();
	std::cout << "Adding the following samples: " << std::endl;
	for(std::string& sample : samplelist) {
    RooStringVar *v = new RooStringVar(sample.c_str(), sample.c_str(), sample.c_str());
		v->Print();
		samplenames.push_back(v);
		inputs->add(*v);
	}

	// RooLagrangianMorphing - setup morphfunc

	// parameters to be fitted
	RooRealVar *kSM  = new RooRealVar("kSM","kSM",1.,0.,2.);
	RooRealVar *ictW = new RooRealVar("ictW","ictW",0.,-10.,10.);
  RooRealVar *ctW  = new RooRealVar("ctW","ctW",0.,-10.,10.);
  RooRealVar *NP0  = new RooRealVar("NP0","NP0",1.);

	// Formula variables for each of the parameters to be fitted
	RooFormulaVar *gSM   = new RooFormulaVar("_gSM", "kSM", RooArgList(*kSM));
	RooFormulaVar *gictW = new RooFormulaVar("_gictW", "ictW", RooArgList(*ictW));
	RooFormulaVar *gctW  = new RooFormulaVar("_gctW", "ctW", RooArgList(*ctW));
	std::vector<RooFormulaVar*> param_formula_vars = {gSM, gictW, gctW};

	// Set of Arguments for production and decay vertices
	RooArgSet *prodCouplings = new RooArgSet("Wtbp");
	RooArgSet *decCouplings  = new RooArgSet("Wtbd");
	for(RooFormulaVar *g : param_formula_vars) {
		g->Print();
		prodCouplings->add(*g);
		decCouplings->add(*g);
	}

	// print the input samples
	inputs->Print();
	prodCouplings->Print();
	decCouplings->Print();

	// Setup Morphing Function
	RooLagrangianMorphFunc* morphexpr = new RooLagrangianMorphFunc("morphfunc", "morphfunc", input_file.c_str(), observable.c_str(), *prodCouplings, *decCouplings, *inputs);
	RooLagrangianMorphPdf* morphfunc = new RooLagrangianMorphPdf("morphfunc", "morphfunc", input_file.c_str(), observable.c_str(), *prodCouplings, *decCouplings, *inputs);
	RooRealVar* x = morphfunc->getObservable();

	x->Print();

	morphfunc->writeCoefficients("test.txt");
	//morphfunc->printSampleWeights();
	std::string sample_weight_file = "test8.txt";
	morphfunc->printSampleWeights(sample_weight_file.c_str());

	std::ifstream f(sample_weight_file.c_str(), std::ifstream::in);
	std::unordered_map<std::string, std::string> morphweightsdict;
	std::string line;
	while(getline(f, line)) {
		std::vector<std::string> splitted_line;
		std::istringstream iss(line);
		std::string temp;
		while(getline(iss, temp, '=')) splitted_line.push_back(temp);
		std::string function = strip(splitted_line[1]);
		replace(function, "_gSM", "@0");
		replace(function, "_gictW", "@1");
		replace(function, "_gctW", "@2");
		replace(function, "nNP0", "@3");
		morphweightsdict[strip(splitted_line[0])] = function;
		std::cout << "Morphing Function = " << std::endl;
		std::cout << function << std::endl;
	}// read weight file loop
	f.close();

	// print morphweightdict
	std::cout << "Morphweightdict : " << std::endl;
	for(auto& element : morphweightsdict) std::cout << element.first << "\t" << element.second << std::endl;
	std::cout << std::endl;

	RooWorkspace *w = new RooWorkspace("w", "workspace");

	unsigned int n_samples = samplelist.size();
	RooArgList *roogaussians = new RooArgList();
	std::vector<RooFormulaVar*> histweights;
	std::vector< std::vector<RooRealVar*> > allroovarbin(n_samples), allroobincontents(n_samples);
	std::vector< std::vector<RooFormulaVar*> > allsamplecontributions(n_samples);
	std::vector< std::vector<RooConstVar*> > allroovarbinerror(n_samples);
	RooArgSet *nuisanceparameter_rooset = new RooArgSet(), *global_observables = new RooArgSet();

	TFile tfile = TFile(input_file.c_str(), "READ");
	TFolder *folder;
	unsigned int binnumbers = 0;

	for(unsigned int i = 0; i < n_samples; i++) {
		std::string sample = samplelist[i];
		
		// extract histogram of observable
		tfile.GetObject(sample.c_str(), folder);
		TH1F* hist_sample = (TH1F*) folder->FindObject(observable.c_str());

		std::string weightkey = "w_" + sample + "_morphfunc";
		RooFormulaVar *histweight = new RooFormulaVar(weightkey.c_str(), morphweightsdict[weightkey].c_str(), RooArgList(*kSM, *ictW, *ctW, *NP0));
		histweights.push_back(histweight);
		w->import(*histweight, RooFit::RecycleConflictNodes());

		if(binnumbers != hist_sample->GetNbinsX() && i > 0) {
			std::cout << "your binnumbers changed from one input to another be very careful" << std::endl;
		}

		binnumbers = hist_sample->GetNbinsX();
		for(unsigned int binnumber = 0; binnumber < binnumbers; binnumber++) {
			// get bin content and error
			double bincontent = hist_sample->GetBinContent(binnumber+1);
			double binerror = hist_sample->GetBinError(binnumber+1);
			// RooVar names
			std::string variablename = sample + "_" + observable + "_" + std::to_string(binnumber+1);
			std::string bincontentname = "bincontent_" + variablename;
			std::string samplecontribution_name = "weighted_" + variablename;
			std::string errorname = "error_" + variablename;
			std::string gausname = "constrain_" + variablename;

			// RooVars
			RooRealVar *roovar = new RooRealVar(variablename.c_str(), variablename.c_str(), bincontent, 0, RooNumber::infinity());
			allroovarbin[i].push_back(roovar);
			RooRealVar *roobincontent = new RooRealVar(bincontentname.c_str(), bincontentname.c_str(), bincontent);
			allroobincontents[i].push_back(roobincontent);
			RooConstVar *roobinerror = new RooConstVar(errorname.c_str(), errorname.c_str(), binerror);
			allroovarbinerror[i].push_back(roobinerror);
			RooFormulaVar *samplecontribution = new RooFormulaVar(samplecontribution_name.c_str(), "@0*@1", RooArgList(*histweight, *roovar));
			allsamplecontributions[i].push_back(samplecontribution);
			RooGaussian *constrain = new RooGaussian(gausname.c_str(), gausname.c_str(), *roovar, *roobincontent, *roobinerror);
			roogaussians->add(*constrain);

			// import RooVars into Workspace
			w->import(*roovar, RooFit::RecycleConflictNodes());
			w->import(*roobincontent, RooFit::RecycleConflictNodes());
			w->import(*roobinerror, RooFit::RecycleConflictNodes());
			w->import(*samplecontribution, RooFit::RecycleConflictNodes());
			w->import(*constrain, RooFit::RecycleConflictNodes());						

			// add bin content variable to nuisance parameter and global observables RooArgSet
			nuisanceparameter_rooset->add(*roovar);
			global_observables->add(*roobincontent);

		}// bin loop

	}// samplelist loop

	tfile.Close();

	//ToDo might change the final mu to the one with all the nuisances included if I got that
	// RooVars for sum of sample contributions in each bin and total normalization
	std::vector<RooFormulaVar*> mutjeslist;
	RooArgList *mutjes = new RooArgList();
	std::string normfunc;
	std::vector<std::string> binexpression;
	for(unsigned int binnumber = 0; binnumber < binnumbers; binnumber++) {
		normfunc += ("+@" + std::to_string(binnumber));
		std::string expression;
		RooArgList *_rooarglist = new RooArgList();
		for(unsigned int i = 0; i < n_samples; i++) {
			_rooarglist->add(*(allsamplecontributions[i][binnumber]));
			expression += ("+@" + std::to_string(i));
		}// sample loop
		expression.replace(0, 1, ""); // remove the first + in the expression
		std::string muname = "mu_bin" + std::to_string(binnumber + 1);
		RooFormulaVar *mubin = new RooFormulaVar(muname.c_str(), expression.c_str(), *_rooarglist);
		mubin->Print();
		mutjeslist.push_back(mubin);
		w->import(*mubin, RooFit::RecycleConflictNodes());
		mutjes->add(*mubin);
	}// bin loop
	normfunc.replace(0, 1, ""); // remove the first + in the expression
	RooFormulaVar *normalization = new RooFormulaVar("Normalization", normfunc.c_str(), *mutjes);
	w->import(*normalization, RooFit::RecycleConflictNodes());

	// RooVars for normalized bin contents
	std::vector<RooFormulaVar*> mubinnormlist;
	for(unsigned int binnumber = 0; binnumber < binnumbers; binnumber++) {
		RooFormulaVar *mubinnorm = new RooFormulaVar(("norm_mu_bin" + std::to_string(binnumber+1)).c_str(), "@0/@1", RooArgList(*(mutjeslist[binnumber]), *normalization));
		mubinnormlist.push_back(mubinnorm);
		w->import(*mubinnorm, RooFit::RecycleConflictNodes());
	}// bin loop

	// Define covariance matrix
	TMatrixDSym *cov_matrix = new TMatrixDSym(7);

	// Asimov test: Validation sample = SM sample
	std::string validation_sample = samplelist[0];
	TFile valid_tfile = TFile(input_file.c_str(), "READ");
	valid_tfile.GetObject(validation_sample.c_str(), folder);
	TH1F* hist_validation = (TH1F*) folder->FindObject(observable.c_str());
	hist_validation->Scale(1./hist_validation->Integral());

	// Import validation bin content and covariance matrix
	std::vector<RooRealVar*> validation_bins;
	for(unsigned int binnumber = 0; binnumber < hist_validation->GetNbinsX(); binnumber++) {

		// bin content
		double bincontent_valid = hist_validation->GetBinContent(binnumber + 1);
		std::cout << "validation bin" << binnumber << "\t" << bincontent_valid << std::endl;
		std::string variablename = "validation_" + observable + "_bin" + std::to_string(binnumber+1);
		RooRealVar *roovar_valid = new RooRealVar(variablename.c_str(), variablename.c_str(), bincontent_valid);
		validation_bins.push_back(roovar_valid);
		w->import(*roovar_valid, RooFit::RecycleConflictNodes());
		
		// bin error
		double binerror_valid = hist_validation->GetBinError(binnumber + 1);
		std::cout << "validation error = " << binerror_valid << std::endl;
		(*cov_matrix)(binnumber, binnumber) = binerror_valid * binerror_valid;
	}// bin loop

	RooArgList *mu_vect = new RooArgList(), *x_vect = new RooArgList();
	std::cout << "Initializing x_data" << std::endl;

	for(unsigned int i = 0; i < cov_matrix->GetNrows(); i++) {
		mu_vect->add(*(mubinnormlist[i]));
		x_vect->add(*(validation_bins[i]));
		validation_bins[i]->Print();
	}

	RooDataSet *x_data = new RooDataSet("asimov", "asimov", RooArgSet(*x_vect));
	x_data->add(RooArgSet(*x_vect));

	RooMultiVarGaussian *normed_multivarpdf = new RooMultiVarGaussian("normed_multivarpdf", "normed_multivarpdf", *x_vect, *mu_vect, *cov_matrix);
	w->import(*normed_multivarpdf, RooFit::RecycleConflictNodes());
	w->import(*cov_matrix);

	roogaussians->add(*normed_multivarpdf);

	RooProdPdf *finalpdf = new RooProdPdf("completepdf", "completepdf", *roogaussians);
	w->import(*finalpdf, RooFit::RecycleConflictNodes());
	w->import(*x_data, RooFit::RecycleConflictNodes());
	
	RooStats::ModelConfig *modelconfig = new RooStats::ModelConfig("ModelConfig", w);
	modelconfig->SetPdf(*finalpdf);

	RooArgSet *pois = new RooArgSet(*kSM,*ictW, *ctW);
	RooArgSet *x_set = new RooArgSet(*x_vect);
	RooArgSet *all_params = new RooArgSet(*pois, *nuisanceparameter_rooset);
	w->saveSnapshot("nominalNuis", *nuisanceparameter_rooset);

	modelconfig->SetParametersOfInterest(*pois);
	modelconfig->SetObservables(*x_set);
	modelconfig->SetNuisanceParameters(*nuisanceparameter_rooset);
	modelconfig->SetGlobalObservables(*global_observables);
	modelconfig->SetSnapshot(*pois);

	w->import(*modelconfig);

	w->writeToFile("test_workspace_y.root");

	//ToDo: 
	//       make a covariance matrix on unnormalised dinges
	//       use jacobian to transform to normalised
	//       make multivargaus
	//       multiply with the alpha params of the MC stat
	std::cout << std::endl << std::endl << std::endl << std::endl;
	w->Print();
	x_data->Print();
	cov_matrix->Print();
	
}// main function

