// morphing
#include "RooLagrangianMorphing/RooLagrangianMorphing.h"

// RooFit
#include "RooStringVar.h"
#include "RooArgList.h"
#include "RooFormulaVar.h"
#include "RooRealVar.h"

// ROOT
#include "TFile.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TFolder.h"
#include "TLegend.h"
#include "TStyle.h"


class RooHCCustomMorphFunc : public RooLagrangianMorphFunc {
  ROOEFTMORPHFUNC(RooHCCustomMorphFunc)
  ClassDef(RooHCCustomMorphFunc,1)
 protected:
  void makeCouplings(){
    RooArgSet kappas("vbfWW");
    RooRealVar* cosa   = new RooRealVar("cosa","cosa",1./sqrt(2));
    RooRealVar* lambda = new RooRealVar("Lambda","Lambda",1000.);
    RooRealVar* kSM    = new RooRealVar("kSM","kSM",1.,0.,2.);
    RooRealVar* kHww   = new RooRealVar("kHww","kHww",0.,-1.,1.);
    RooRealVar* kAww   = new RooRealVar("kAww","kAww",0.,-1.,1.);
    kappas.add(*cosa);
    kappas.add(*lambda);
    kappas.add(*kSM);
    kappas.add(*kHww);
    kappas.add(*kAww);
    RooArgSet prodCouplings("vbf");
    RooArgSet decCouplings("hww");
    prodCouplings.add(*(new RooFormulaVar("_gSM"  ,"cosa*kSM",                        RooArgList(*cosa,*kSM))));
    prodCouplings.add(*(new RooFormulaVar("_gHww" ,"cosa*kHww/Lambda",                RooArgList(*cosa,*kHww,*lambda))));
    prodCouplings.add(*(new RooFormulaVar("_gAww" ,"sqrt(1-(cosa*cosa))*kAww/Lambda", RooArgList(*cosa,*kAww,*lambda))));
    decCouplings.add (*(new RooFormulaVar("_gSM"  ,"cosa*kSM",                        RooArgList(*cosa,*kSM))));
    decCouplings.add (*(new RooFormulaVar("_gHww" ,"cosa*kHww/Lambda",                RooArgList(*cosa,*kHww,*lambda))));
    decCouplings.add (*(new RooFormulaVar("_gAww" ,"sqrt(1-(cosa*cosa))*kAww/Lambda", RooArgList(*cosa,*kAww,*lambda))));
    this->setup(kappas,prodCouplings,decCouplings);
  }
};
ClassImp(RooHCCustomMorphFunc);


int main(int argc, const char* argv[]){
  
  // define process identifier, input file and observable
  std::string identifier("vbfWW"); // avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  std::string infilename("input/vbfhwwlvlv_3d.root"); // give the input file name here
  std::string observable("twoSelJets/dphijj"); // name of the observable to be used (histogram name)

  // these are the names of the input samples 
  std::vector<std::string> samplelist = {"kAwwkHwwkSM0","kAwwkHwwkSM1","kAwwkHwwkSM10","kAwwkHwwkSM11","kAwwkHwwkSM12","kAwwkHwwkSM13","kAwwkHwwkSM2","kAwwkHwwkSM3","kAwwkHwwkSM4","kAwwkHwwkSM5","kAwwkHwwkSM6","kAwwkHwwkSM7","kAwwkHwwkSM8","kAwwkHwwkSM9","kSM0"};
  // these are the validation samples: "v0","v1","v2","v3","v4","v5","v6","v7","v8","v9"

  // push all the input samples in a RooArgList
  RooArgList inputs;
  for(auto const& sample: samplelist) {
    RooStringVar* v = new RooStringVar(sample.c_str(),sample.c_str(),sample.c_str());
    inputs.add(*v);
  }
    
  RooLagrangianMorphFunc * morphfunc = new RooHCCustomMorphFunc("morphfunc","morphfunc",infilename.c_str(),observable.c_str(),inputs);
  
  // morph to the validation sample v1
  std::string validationsample("v1");
  morphfunc->setParameters(validationsample.c_str());
  TH1* morphing = morphfunc->createTH1("morphing");

  // open the input file to get the validation histogram for comparison
  TFile* file = TFile::Open(infilename.c_str(),"READ");
  TFolder* folder = 0;
  file->GetObject(validationsample.c_str(),folder);
  TH1* validation = dynamic_cast<TH1*>(folder->FindObject(observable.c_str()));
  validation->SetDirectory(NULL);
  validation->SetTitle(validationsample.c_str());
  file->Close();

  // plot everything
  TCanvas* plot = new TCanvas("plot");
  plot->cd();
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  morphing->GetXaxis()->SetTitle(observable.c_str());
  morphing->SetLineColor(kRed);
  morphing->SetFillColor(kRed);
  morphing->Draw("E3");
  validation->Draw("SAME");
  TLegend* leg = new TLegend(0.7,0.7,0.9,0.9);
  leg->AddEntry(morphing);
  leg->AddEntry(validation);
  leg->Draw();
  plot->SaveAs("plot.pdf","pdf");
  
  return 0;
}

