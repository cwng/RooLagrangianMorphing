#!/bin/env python
import argparse
import os

def main():
  import ROOT 

  # define process identifier, input file and observable
  identifier = "vbfWW" # avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  infilename = "input/vbfhwwlvlv_3d.root" # give the input file name here
  observable = "twoSelJets/dphijj" # name of the observable to be used (histogram name)

  # get morphfunc from workspace
  workspacefile = ROOT.TFile.Open("workspace.root","READ");
  if not workspacefile or not workspacefile.IsOpen():
    print("unable to open workspace file")
    exit(0)
  w = workspacefile.Get("w");
  morphfunc = w.obj(identifier)
  workspacefile.Close()

  # morph to the validation sample v1
  validationsample = "v1"
  morphfunc.setParameters(validationsample)
  morphing = morphfunc.createTH1("morphing")

  # open the input file to get the validation histogram for comparison
  tfile = ROOT.TFile.Open(infilename,"READ")
  folder = tfile.Get(validationsample)
  validation = folder.FindObject(observable)
  validation.SetDirectory(0)
  validation.SetTitle(validationsample)
  tfile.Close()

  # setup the fit
  target = ROOT.RooLagrangianMorphing.makeDataHistogram(validation,morphfunc.getObservable(),"validation") # convert the target to a RooDataHist
  morphfunc.setParameters(validationsample);
  morphfunc.setParameterConstant("Lambda",True);
  morphfunc.setParameterConstant("cosa",True);
  morphfunc.randomizeParameters(2); # randomize the parameters by 2 standard deviations to give the fit something to do
  morphfunc.printParameters();
  morphfunc.getPdf().fitTo(target,ROOT.RooFit.SumW2Error(True),ROOT.RooFit.Optimize(False)); # run the fit
  morphfunc.printParameters();
  fitresult = morphfunc.createTH1("fit result");

  # plot everything
  plot = ROOT.TCanvas("plot")
  plot.cd()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gStyle.SetOptTitle(0)
  morphing.GetXaxis().SetTitle(observable)
  morphing.SetLineColor(ROOT.kRed)
  morphing.SetFillColor(ROOT.kRed)
  morphing.Draw("E3")
  fitresult.SetLineColor(ROOT.kBlack);
  fitresult.SetMarkerColor(ROOT.kBlack);
  fitresult.SetMarkerStyle(20);
  fitresult.Draw("PSAME");
  validation.Draw("SAME")
  leg = ROOT.TLegend(0.7,0.7,0.9,0.9)
  leg.AddEntry(fitresult)
  leg.AddEntry(morphing)
  leg.AddEntry(validation)
  leg.Draw()
  plot.SaveAs("plot.pdf","pdf")

if __name__ == "__main__":
  # some argument parsing to provide the path to the RooLagrangianMorphFunc
  parser = argparse.ArgumentParser("morphing example")
  args = parser.parse_args()
  # load all required libraries
  from ROOT import gSystem
  gSystem.Load("libRooFit")
  gSystem.Load("libRooLagrangianMorphing.so")
  # call the main function
  main()
