// morphing
#include "RooLagrangianMorphing/RooLagrangianMorphing.h"

// RooFit
#include "RooDataHist.h"
#include "RooWorkspace.h"

// ROOT
#include "TFile.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TFolder.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TKey.h"

int main(int argc, const char* argv[]){

  // define process identifier, input file and observable
  std::string identifier("vbfWW"); // avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  std::string infilename("input/vbfhwwlvlv_3d.root"); // give the input file name here
  std::string observable("twoSelJets/dphijj"); // name of the observable to be used (histogram name)

  // get morphfunc from workspace
  TFile* workspacefile = TFile::Open("workspace.root","READ");
  if(!workspacefile || !workspacefile->IsOpen()){
    std::cout << "unable to open file!" << std::endl;
    exit(1);
  }
  RooWorkspace* w = (RooWorkspace*)(workspacefile->Get("w"));
  RooLagrangianMorphPdf* morphfunc = dynamic_cast<RooLagrangianMorphPdf*>(w->obj(identifier.c_str()));
  workspacefile->Close();

  // morph to the validation sample v1
  std::string validationsample("v1");
  morphfunc->setParameters(validationsample.c_str());
  TH1* morphing = morphfunc->createTH1("morphing");

  // open the input file to get the validation histogram for comparison
  TFile* file = TFile::Open(infilename.c_str(),"READ");
  TFolder* folder = 0;
  file->GetObject(validationsample.c_str(),folder);
  TH1* validation = dynamic_cast<TH1*>(folder->FindObject(observable.c_str()));
  validation->SetDirectory(NULL);
  validation->SetTitle(validationsample.c_str());
  file->Close();

  // setup the fit
  RooDataHist* target = RooLagrangianMorphing::makeDataHistogram(validation,morphfunc->getObservable(),"validation"); // convert the target to a RooDataHist
  morphfunc->setParameters(validationsample.c_str());
  morphfunc->setParameterConstant("Lambda",true);
  morphfunc->setParameterConstant("cosa",true);
  morphfunc->randomizeParameters(2); // randomize the parameters by 2 standard deviations to give the fit something to do
  morphfunc->printParameters();
  morphfunc->getPdf()->fitTo(*target,RooFit::SumW2Error(true),RooFit::Optimize(false)); // run the fit
  morphfunc->printParameters();
  TH1* fitresult = morphfunc->createTH1("fit result");

  // plot everything
  TCanvas* plot = new TCanvas("plot");
  plot->cd();
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  morphing->GetXaxis()->SetTitle(observable.c_str());
  morphing->SetLineColor(kRed);
  morphing->SetFillColor(kRed);
  morphing->Draw("E3");
  fitresult->SetLineColor(kBlack);
  fitresult->SetMarkerColor(kBlack);
  fitresult->SetMarkerStyle(20);
  fitresult->Draw("PSAME");
  validation->Draw("SAME");
  TLegend* leg = new TLegend(0.7,0.7,0.9,0.9);
  leg->AddEntry(fitresult);
  leg->AddEntry(morphing);
  leg->AddEntry(validation);
  leg->Draw();
  plot->SaveAs("plot.pdf","pdf");
  return 0;
}
