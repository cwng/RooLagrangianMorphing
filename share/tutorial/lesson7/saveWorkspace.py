#!/bin/env python
import argparse
import os

def main():
  import ROOT

  # define process identifier, input file and observable
  identifier = "vbfWW" # avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  infilename = "input/vbfhwwlvlv_3d.root" # give the input file name here
  observable = "twoSelJets/dphijj" # name of the observable to be used (histogram name)

  # these are the names of the input samples
  samplelist = ["kAwwkHwwkSM0","kAwwkHwwkSM1","kAwwkHwwkSM10","","kAwwkHwwkSM11","kAwwkHwwkSM12","kAwwkHwwkSM13","kAwwkHwwkSM2","kAwwkHwwkSM3","kAwwkHwwkSM4","kAwwkHwwkSM5","kAwwkHwwkSM6","kAwwkHwwkSM7","kAwwkHwwkSM8","kAwwkHwwkSM9","kSM0"]
  # these are the validation samples: "v0","v1","v2","v3","v4","v5","v6","v7","v8","v9"

  # setup morphing function with workspace
  w = ROOT.RooWorkspace("w","morphfunc");
  wsstr = "RooHC"+identifier+"MorphPdf::"+identifier+"('"+infilename+"','"+observable+"','"+samplelist[0]+"',{"+",".join(["'"+x+"'" for x in samplelist])+"})"
  w.factory(wsstr)
  morphfunc = w.obj(identifier)
  if not morphfunc:
    print("failure to create morphing function!")
  else:
    w.writeToFile("workspace.root");

if __name__ == "__main__":
  # some argument parsing to provide the path to the RooLagrangianMorphFunc
  parser = argparse.ArgumentParser("morphing example")
  args = parser.parse_args()
  # load all required libraries
  from ROOT import gSystem
  gSystem.Load("libRooFit")
  gSystem.Load("libRooLagrangianMorphing.so")
  # call the main function
  main()
