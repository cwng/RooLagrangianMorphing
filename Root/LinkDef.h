//this file is -*- c++ -*-
#include "RooLagrangianMorphing/Floats.h"
#include "RooLagrangianMorphing/LinearCombination.h"
#include "RooLagrangianMorphing/RooLagrangianMorphing.h"
#include "RooLagrangianMorphing/RooLagrangianMorphOptimizer.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ class RooLagrangianMorphing::RooLagrangianMorphBase<RooAbsReal>+;
#pragma link C++ class RooLagrangianMorphing::RooLagrangianMorphBase<RooAbsPdf>+;
#pragma link C++ class RooLagrangianMorphFunc+;
#pragma link C++ class RooLagrangianMorphPdf+;
#pragma link C++ class RooLagrangianMorphOptimizer+;
#pragma link C++ class RooHCggfWWMorphFunc+;
#pragma link C++ class RooHCvbfWWMorphFunc+;
#pragma link C++ class RooHCggfZZMorphFunc+;
#pragma link C++ class RooHCvbfZZMorphFunc+;
#pragma link C++ class RooHCvbfMuMuMorphFunc+;
#pragma link C++ class RooHCggfWWMorphPdf+;
#pragma link C++ class RooHCvbfWWMorphPdf+;
#pragma link C++ class RooHCggfZZMorphPdf+;
#pragma link C++ class RooHCvbfZZMorphPdf+;
#pragma link C++ class RooHCvbfMuMuMorphPdf+;
#pragma link C++ class RooSMEFTggfWWMorphFunc+;
#pragma link C++ class RooSMEFTvbfWWMorphFunc+;
#pragma link C++ class RooSMEFTggfWWMorphPdf+;
#pragma link C++ class RooSMEFTvbfWWMorphPdf+;
#pragma link C++ class RooSMEFTggfMorphFunc+;
#pragma link C++ class RooSMEFTvbfMorphFunc+;
#pragma link C++ class RooSMEFTggfMorphPdf+;
#pragma link C++ class RooSMEFTvbfMorphPdf+;
#pragma link C++ typedef RooLagrangianMorphing::ParamSet;
#pragma link C++ typedef RooLagrangianMorphing::ParamMap;
#pragma link C++ namespace RooLagrangianMorphing;
#pragma link C++ function RooLagrangianMorphing::append;
#ifdef USE_MULTIPRECISION_LC
#pragma link C++ class RooLagrangianMorphing::LinearCombination+;
#endif

#endif // __CINT__

